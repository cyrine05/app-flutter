import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class Stockdatabase {
  static Database? database;

  static Future<Database> getDatabase() async {
    return database ??= await initDatabase();
  }

  static Future<Database> initDatabase() async {
    return await openDatabase(
        join(await getDatabasesPath(), "gestionComposant.db"),
        version: 1,
        onCreate: _onCreate);
  }

  static Future _onCreate(Database db, int version) async {
    await db.execute('''CREATE TABLE FAMILY (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT UNIQUE NOT NULL
      )
      ''');
    await db.execute('''CREATE TABLE COMPONENTS  (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      namec TEXT NOT NULL,
      dateacq DATE NOT NULL,
      quantite INTEGER NOT NULL,
      namefa TEXT NOT NULL,
      FOREIGN KEY(namefa) REFERENCES FAMILY(namef)
      )
      ''');
    await db.execute('''CREATE TABLE LOANS  (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      nameco TEXT NOT NULL,
      quantitec INTEGER NOT NULL,
      nom TEXT NOT NULL,
      prenom TEXT NOT NULL,
      num1 INTEGER NOT NULL,
      num2 INTEGER NOT NULL,
      dater DATE ,
      etat TEXT ,
      valid BOOLEAN DEFAULT(TRUE)
      
      )
      ''');
  }
}
