import 'dart:async';

import 'package:flutter_project/back/dbcreation.dart';

import 'package:sqflite/sqflite.dart';

import 'package:flutter_project/models/loansmodel.dart';

class Loansservice {
  Future<void> insertloans(Loans loan) async {
    Database db = await Stockdatabase.getDatabase();

    await db.insert(
      'Loans',
      loan.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    print(loan.toMap());
  }

  static Future<List<Loans>> getAllloans() async {
    Database db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> maploan = await db.query("Loans");
    List<Loans> allloan = [];
    maploan.forEach((element) => allloan.add(Loans.fromMap(element)));
    return allloan;
  }
}
