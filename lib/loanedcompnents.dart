import 'package:flutter/material.dart';

class LoanedComponentsPage extends StatelessWidget {
  const LoanedComponentsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Center(
        child: Text("LoanedComponents Page"),
      ),
    );
  }
}
