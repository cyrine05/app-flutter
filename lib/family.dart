import 'package:flutter/material.dart';
import 'package:flutter_project/back/familyservice.dart';
import 'package:flutter_project/models/familymodel.dart';

class ManageFamilyPage extends StatefulWidget {
  ManageFamilyPage({Key? key}) : super(key: key);

  @override
  _ManageFamilyState createState() => _ManageFamilyState();
}

class _ManageFamilyState extends State<ManageFamilyPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  Familyservice myservice = Familyservice();
  bool _validate = false;
  bool _validatep = false;
  bool _validateu = false;
  bool _validatepa = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: [
            SizedBox(
              width: 400,
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Family Name',
                    errorText: _validate ? 'Value Can\'t Be Empty' : null),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            ElevatedButton(
              style: TextButton.styleFrom(
                primary: Colors.white,
              ),
              child: const Text('Login'),
              onPressed: () async{
                nameController.text.isEmpty
                      ? _validate = true
                      : _validate = false;
                   await myservice.insertfamille(
                      Family(namef: nameController.text));
                setState(()  {
                  
                });
              },
            ),
            
          ],
        ),
      ),
    );
  }
}
