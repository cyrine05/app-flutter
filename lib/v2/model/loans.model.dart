class Loan {
  int? id;
  final int quantity;
  final int member;
  final int componentId;
  bool isReturned;
  String? returnedAt;
  String? state;

  Loan({
    id,
    required this.quantity,
    required this.member,
    required this.componentId,
    this.isReturned = false,
    this.returnedAt = "",
    this.state = "intact",
  });

  Map<String, dynamic> toMap() {
    return {
      'quantity': quantity,
      'member': member,
      'returnedAt': returnedAt,
      'state': state,
      'isReturned': isReturned,
      'componentId': componentId,
    };
  }

  static Loan fromMap(Map<String, dynamic> json) {
    return Loan(
        id: json['id'],
        quantity: json['quantity'],
        member: json['member'],
        returnedAt: json['returnedAt'],
        state: json['state'],
        componentId: json['componentId'],
        isReturned: json['isReturned']);
  }
}
