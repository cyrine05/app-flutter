class Family {
  int id;
  String name;

  Family({
    this.id = 0,
    required this.name,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name,
    };
  }

  static Family fromMap(Map<String, dynamic> json) {
    return Family(
      name: json['name'].toString(),
      id: json['id'],
    );
  }
}
