class LoanedObject {
  int id;
  String firstName;
  String lastName;
  int firstNumber;
  int secondNumber;
  String name;
  int componentId;
  int quantity;

  LoanedObject({
    this.id = 0,
    required this.firstName,
    required this.lastName,
    required this.firstNumber,
    required this.secondNumber,
    required this.name,
    required this.componentId,
    required this.quantity,
  });

  static LoanedObject fromMap(Map<String, dynamic> json) {
    return LoanedObject(
      id: json['id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      firstNumber: json['firstNumber'],
      secondNumber: json['secondNumber'],
      name: json['name'],
      componentId: json['componentId'],
      quantity: json['quantity'],
    );
  }
}
