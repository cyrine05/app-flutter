class Component {
  int? id;
  final String name;
  final int quantity;
  final String addedAt;
  final int family;

  Component({
    this.id,
    required this.name,
    required this.addedAt,
    required this.quantity,
    required this.family,
  });
  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'addedAt': addedAt,
      'quantity': quantity,
      'family': family,
    };
  }

  static Component fromMap(Map<String, dynamic> json) {
    return Component(
        id: json['id'],
        name: json['name'],
        addedAt: json['addedAt'].toString(),
        quantity: json['quantity'],
        family: json['family']);
  }
}
