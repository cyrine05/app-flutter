import 'package:flutter/material.dart';
import 'package:flutter_project/v2/model/component.model.dart';
import 'package:flutter_project/v2/model/family.model.dart';
import 'package:flutter_project/v2/screen/component/add.component.dart';
import 'package:flutter_project/v2/screen/family/add.family.dart';
import 'package:flutter_project/v2/service/component.service.dart';
import 'package:flutter_project/v2/service/family.service.dart';

import 'package:get/get.dart';

class ComponentsListPage extends StatefulWidget {
  ComponentsListPage({Key? key}) : super(key: key);

  @override
  _ComponentsListState createState() => _ComponentsListState();
}

class _ComponentsListState extends State<ComponentsListPage> {
  ComponentService myservice = ComponentService();
  AddComponentPage addcomponent = AddComponentPage();

  TextEditingController quantityController = TextEditingController();
  TextEditingController _textController = TextEditingController();

  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xff03dac6),
        foregroundColor: Colors.black,
        onPressed: () {
          Get.to(addcomponent);
        },
        child: Icon(Icons.add),
      ),
      body: Container(
        child: FutureBuilder<List<Component>>(
          future: myservice.loadAllComponenets(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Component>> snapshot) {
            if (!snapshot.hasData) {
              return Text("NO DATA");
            }
            return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: <Widget>[
                      Card(
                        elevation: 8,
                        margin: const EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
                              leading: const Icon(Icons.computer, size: 48),
                              trailing: InkWell(
                                child: const Icon(Icons.edit, size: 20),
                                onTap: () {
                                  Get.defaultDialog(
                                      title: "Update quantity",
                                      content: TextField(
                                        controller: quantityController,
                                        decoration: InputDecoration(
                                            border: const OutlineInputBorder(),
                                            labelText: 'Update quantity',
                                            errorText: _validate
                                                ? 'Value Can\'t Be Empty'
                                                : null),
                                      ),
                                      textConfirm: "ADD",
                                      textCancel: "CANCEL",
                                      onConfirm: () async {
                                        quantityController.text.isEmpty
                                            ? _validate = true
                                            : _validate = false;
                                        await myservice.updateQuantity(
                                            snapshot.data![index].id!,
                                            int.parse(quantityController.text) +
                                                snapshot.data![index].quantity);
                                        Get.back();
                                        setState(() {});
                                      });
                                },
                              ),
                              title: FittedBox(
                                child: Row(
                                  children: [
                                    Text("name:" + snapshot.data![index].name,
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600)),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                        snapshot.data![index].quantity
                                            .toString(),
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600)),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                        "AdedAt: " +
                                            snapshot.data![index].addedAt
                                                .toString(),
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600)),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                });
          },
        ),
      ),
    );
  }

  rechrcheWidget() {
    return FutureBuilder<List<Component>>(
      future: ComponentService().searchComponenetByName(_textController.text),
      builder: (BuildContext context, AsyncSnapshot<List<Component>> snapshot) {
        Widget children;
        if (snapshot.hasData) {
          children = TextField(
              controller: _textController,
              onChanged: (text) {
                setState(() {
                  _textController.text = text;
                });
                if (snapshot.data!.length == 0 &&
                    _textController.text.isNotEmpty) {
                  Expanded(
                    child: Text('Aucune donnée'),
                  );
                } else {
                  Expanded(
                    child: ListView.builder(
                        itemCount: snapshot.data!.length,
                        itemBuilder: (BuildContext context, index) {
                          return Container(
                            height: 50,
                            child: Row(
                              children: [
                                Text(snapshot.data![index].name),
                                Text(snapshot.data![index].quantity.toString()),
                              ],
                            ),
                          );
                        }),
                  );
                }
              });
        } else {
          children = const Text('No components');
        }
        return children;
      },
    );
  }
}
