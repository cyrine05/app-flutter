import 'package:flutter/material.dart';
import 'package:flutter_project/back/familyservice.dart';
import 'package:flutter_project/back/loansservice.dart';
import 'package:flutter_project/models/componentsmodel.dart';
import 'package:flutter_project/models/familymodel.dart';
import 'package:flutter_project/models/loansmodel.dart';
import 'package:get/get.dart';

import 'back/componentsservice.dart';

class ManageLoansPage extends StatefulWidget {
  ManageLoansPage({Key? key}) : super(key: key);

  @override
  _ManageLoansState createState() => _ManageLoansState();
}

class _ManageLoansState extends State<ManageLoansPage> {
  final itemss = [""];

  TextEditingController nomController = TextEditingController();
  TextEditingController quantiteController = TextEditingController();
  TextEditingController prenomController = TextEditingController();
  TextEditingController num1Controller = TextEditingController();
  TextEditingController num2Controller = TextEditingController();

  Loansservice myservice = Loansservice();
  bool _validate = false;
  bool _validatep = false;
  bool _validateu = false;
  bool _validatepa = false;
  String? value;
  String? componentname;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: FutureBuilder<List<Components>>(
          future: Componentsservice.getAllcomponent(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Components>> snapshot) {
            Widget children;
            if (snapshot.hasData) {
              children = DropdownButton<String>(
                value: componentname,
                icon: const Icon(Icons.arrow_downward),
                iconSize: 24,
                hint: const Text("CHOOSE component"),
                elevation: 16,
                style: const TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                items: snapshot.data!.map<DropdownMenuItem<String>>((e) {
                  return DropdownMenuItem<String>(
                    alignment: AlignmentDirectional.center,
                    value: e.namec,
                    child: Text(e.namec),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    componentname = value;
                  });
                },
              );
            } else {
              children = const Text('No component');
            }
            return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(
                    height: 120,
                  ),
                  SizedBox(
                    width: 400,
                    child: TextField(
                      controller: quantiteController,
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'quantité',
                          errorText:
                              _validate ? 'Value Can\'t Be Empty' : null),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  SizedBox(
                    width: 400,
                    child: TextField(
                      controller: nomController,
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'Nom',
                          errorText:
                              _validate ? 'Value Can\'t Be Empty' : null),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  SizedBox(
                    width: 400,
                    child: TextField(
                      controller: prenomController,
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'prenom',
                          errorText:
                              _validate ? 'Value Can\'t Be Empty' : null),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  SizedBox(
                    width: 400,
                    child: TextField(
                      controller: num1Controller,
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'num1',
                          errorText:
                              _validate ? 'Value Can\'t Be Empty' : null),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  SizedBox(
                    width: 400,
                    child: TextField(
                      controller: num2Controller,
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'num2',
                          errorText:
                              _validate ? 'Value Can\'t Be Empty' : null),
                    ),
                  ),
                  children,
                  const SizedBox(
                    height: 80,
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        var obj = Loans(
                          nameco: componentname.toString(),
                          quantitec: int.parse(quantiteController.text),
                          nom: nomController.text,
                          prenom: prenomController.text,
                          num1: int.parse(num1Controller.text),
                          num2: int.parse(num2Controller.text),
                          valid: true,
                        );

                        await myservice.insertloans(obj);
                      },
                      child: const Text("Confirm")),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
