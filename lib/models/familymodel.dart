class Family {
  String? namef;
  Family({required this.namef});

  Map<String, dynamic> toMap() {
    return {
      'namef': namef,
    };
  }

  static Family fromMap(Map<String, dynamic> json) {
    return Family(
      namef: json['namef'],
    );
  }
}
