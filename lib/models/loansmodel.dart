class Loans {
  final String nameco;
  final int quantitec;
  final String nom;
  final String prenom;
  final int num1;
  final int num2;
  String? dater = "";
  String? etat = "";
  final bool valid;

  Loans({
    required this.nameco,
    required this.quantitec,
    required this.nom,
    required this.prenom,
    required this.num1,
    required this.num2,
    this.dater,
    this.etat,
    required this.valid,
  });

  Map<String, dynamic> toMap() {
    return {
      'nameco': nameco,
      'quantitec': quantitec,
      'nom': nom,
      'prenom': prenom,
      'num1': num1,
      'num2': num2,
      'dater': dater,
      'etat': etat,
    };
  }

  static Loans fromMap(Map<String, dynamic> json) {
    return Loans(
        nameco: json['nameco'],
        quantitec: json['quantitec'],
        nom: json['nom'],
        prenom: json['prenom'],
        num1: json['num1'],
        num2: json['num2'],
        dater: json['dater'],
        etat: json['etat'],
        valid: json['valid']);
  }
}
